#include <stdio.h>

/*E1: Dado um número inteiro de 3 algarismos, invertera ordem de seus algarismos. 
Os três algarismos do número dado são diferentes de zero.*/

int main(){
    int a,invt,u,d,c;
    
    scanf("%i", &a);
    u = a % 10;
    d = (a % 100) - u;
    c = (a % 1000) - d - u;
    invt = (u * 100) + d + (c/100);

    printf("%i\n", invt);

    return 0;
}