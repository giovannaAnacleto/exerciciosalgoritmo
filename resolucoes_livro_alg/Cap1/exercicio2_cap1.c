#include <stdio.h>


int main(){
    int a,alg1,alg2,alg3,alg4,novo;

    scanf("%i", &a);
    alg1 = a%10;
    alg2 = ((a%100) - alg1) /10;
    alg3 = ((a%1000) - alg2 - alg1) / 100;
    alg4 = (alg3 + (alg2*3) + (alg1*5)) % 7;
    novo = (alg3*1000) + (alg2 * 100) + (alg1*10) + alg4;

    printf("%i\n", novo);
    
    return 0;
}