#include <stdio.h>

int main(){
    int a,graus,min,seg;
    
    scanf("%i", &a);
    graus = a / 3600;
    min = a / 60;
    seg = a % 60;

    printf("%i graus, %i minutos, %i segundos.\n", graus, min,seg);
    return 0;
}